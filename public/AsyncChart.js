var AsyncChart = (function() {
  var chartData;
  var decoder = new TextDecoder();
  var dataTable;
  var chart;

  function load(data) {
    google.charts.load('current', {'packages':['timeline']});
    google.charts.setOnLoadCallback(function() {
      initializeChart();
      startProcessing(data);
    });
  }

  function startProcessing(data) {
    fetch(data.endpoint, {
      method: "POST",
      body: JSON.stringify({ 'poem': data.poem })
    }).then(function(response) {
      parseStream(response.body, response.body.getReader());
    });
  }

  function parseStream(stream, reader) {
    reader.read().then(function(chunk) {
      if (chunk.done) {
        console.log("Done.");
        return;
      }

      processChunk(chunk);
      parseStream(stream, reader);
    });
  }

  function processChunk(chunk) {
    var decodedChunk = decoder.decode(chunk.value);
    var items = parseNDJSON(decodedChunk);
    items.forEach(processItem);
  }

  function parseNDJSON(ndjsonString) {
    var jsonLines = ndjsonString.trim().split('\n');
    return jsonLines.map(function(line) {
      return JSON.parse(line);
    });
  }

  function processItem(item) {
    if (item.rowData) {
      updateChart(item.rowData);
    }

    if (item.output) {
      Output.load(item);
    }
  }

  function initializeChart() {
    var container = document.getElementById('timeline');
    chart = new google.visualization.Timeline(container);
    dataTable = new google.visualization.DataTable();

    dataTable.addColumn({ type: 'string', id: 'Service' });
    dataTable.addColumn({ type: 'string', id: 'Item' });
    dataTable.addColumn({ type: 'string', id: 'style', role: 'style' });
    dataTable.addColumn({ type: 'date', id: 'Start' });
    dataTable.addColumn({ type: 'date', id: 'End' });
  }

  function updateChart(rowData) {
    rowData[3] = new Date(parseInt(rowData[3]));
    rowData[4] = new Date(parseInt(rowData[4]));
    dataTable.addRow(rowData);
    chart.draw(dataTable);
  }

  return {
    load: load
  };
})();
