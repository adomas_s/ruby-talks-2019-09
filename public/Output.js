var Output = (function() {
  function load(data) {
    var outputElement = document.getElementById("output");
    outputElement.innerHTML = data.output;
  };

  return {
    load: load
  };
})();
