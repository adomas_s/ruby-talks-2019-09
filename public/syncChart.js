var SyncChart = (function() {
  var chartData;

  function load(data) {
    chartData = data;

    google.charts.load('current', {'packages':['timeline']});
    google.charts.setOnLoadCallback(drawChart);
  }

  function drawChart() {
    var container = document.getElementById('timeline');
    var chart = new google.visualization.Timeline(container);
    var dataTable = new google.visualization.DataTable();

    dataTable.addColumn({ type: 'string', id: 'Service' });
    dataTable.addColumn({ type: 'string', id: 'Item' });
    dataTable.addColumn({ type: 'string', id: 'style', role: 'style' });
    dataTable.addColumn({ type: 'date', id: 'Start' });
    dataTable.addColumn({ type: 'date', id: 'End' });

    chartData.forEach(function(rowData) {
      rowData[3] = new Date(parseInt(rowData[3]));
      rowData[4] = new Date(parseInt(rowData[4]));
      dataTable.addRow(rowData);
    });
    chart.draw(dataTable);
  }

  return {
    load: load
  };
})();
