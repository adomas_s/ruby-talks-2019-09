### Setup

```
bundle install
```

### Running the application

```
rackup
```

### Accessing the application in browser

```
http://localhost:9292
```
