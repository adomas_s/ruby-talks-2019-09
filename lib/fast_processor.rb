require "concurrent"
require "services/reverse_service"
require "services/uppercase_service"
require "processor_output"

class FastProcessor
  include ProcessorOutput

  POOL_SIZE = 4

  def initialize
    @semaphore = Mutex.new
    @thread_pool = Concurrent::FixedThreadPool.new(POOL_SIZE)
  end

  def process(poem)
    @poem = poem
    @result = []

    @benchmark = Benchmark.measure do
      words.each_with_index do |word, i|
        thread_pool.post do
          step1 = ReverseService.new.call(word)
          step2 = UppercaseService.new.call(step1)

          semaphore.synchronize do
            result[i] = step2
          end
        end
      end

      thread_pool.shutdown
      thread_pool.wait_for_termination
    end
  end

  private

  attr_reader :benchmark, :poem, :result, :semaphore, :thread_pool

  def words
    poem.split(" ")
  end
end
