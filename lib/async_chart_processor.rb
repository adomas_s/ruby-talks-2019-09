require "concurrent"
require "services/reverse_service"
require "services/uppercase_service"
require "processor_output"

class AsyncChartProcessor
  include ProcessorOutput

  POOL_SIZE = 4

  attr_reader :chart_data, :result

  def initialize
    @semaphore = Mutex.new
    @thread_pool = Concurrent::FixedThreadPool.new(POOL_SIZE)
  end

  def process(poem, stream)
    @poem = poem
    @result = []
    @chart_data = []

    @benchmark = Benchmark.measure do
      words.each_with_index do |word, i|
        thread_pool.post do
          start1 = Time.now
          step1 = ReverseService.new.call(word)
          finish1 = Time.now

          semaphore.synchronize do
            stream << js_row_data(["ReverseService", word, color, start1, finish1])
          end

          start2 = Time.now
          step2 = UppercaseService.new.call(step1)
          finish2 = Time.now

          semaphore.synchronize do
            result[i] = step2
            stream << js_row_data(["UppercaseService", word, color, start2, finish2])
          end
        end
      end

      thread_pool.shutdown
      thread_pool.wait_for_termination
    end

    stream << js_output_data(output)
  end

  private

  attr_reader :benchmark, :poem, :semaphore, :thread_pool

  def words
    poem.split(" ")
  end

  def js_row_data(array)
    JSON.generate(rowData: convert_times(array)) + "\n"
  end

  def js_output_data(string)
    JSON.generate(output: string) + "\n"
  end

  def convert_times(array)
    array.map do |item|
      case item
      when Time
        js_time(item)
      else
        item
      end
    end
  end

  def js_time(time)
    time.strftime("%s%3N")
  end

  def color
    "#fed551"
  end
end
