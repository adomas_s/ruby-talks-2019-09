require "concurrent"
require "services/reverse_service"
require "services/uppercase_service"
require "processor_output"

class SyncChartProcessor
  include ProcessorOutput

  POOL_SIZE = 4

  attr_reader :chart_data, :result

  def initialize
    @semaphore = Mutex.new
    @thread_pool = Concurrent::FixedThreadPool.new(POOL_SIZE)
  end

  def process(poem)
    @poem = poem
    @result = []
    @chart_data = []

    @benchmark = Benchmark.measure do
      words.each_with_index do |word, i|
        thread_pool.post do
          start1 = Time.now
          step1 = ReverseService.new.call(word)
          finish1 = Time.now

          start2 = Time.now
          step2 = UppercaseService.new.call(step1)
          finish2 = Time.now

          semaphore.synchronize do
            result[i] = step2
            @chart_data << ["ReverseService", word, color, js_time(start1), js_time(finish1)]
            @chart_data << ["UppercaseService", word, color, js_time(start2), js_time(finish2)]
          end
        end
      end

      thread_pool.shutdown
      thread_pool.wait_for_termination
    end
  end

  private

  attr_reader :benchmark, :poem, :semaphore, :thread_pool

  def words
    poem.split(" ")
  end

  def js_time(time)
    time.strftime("%s%3N")
  end

  def color
    "#fed551"
  end
end
