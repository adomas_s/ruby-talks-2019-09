module ProcessorOutput
  def output
    out = []
    out << format("Processed poem:\n\n%s\n", poem)
    out << format("#{words.size} words processed in %4.1fs", benchmark.real)
    out << format("Speed: %4.1f words per second", words.size / benchmark.real)
    out << format("Result:\n\n%s", result.join(" "))
    out.join("\n")
  end
end
