class UppercaseService
  def call(word)
    sleep word.size * 0.1
    word.upcase
  end
end
