class ReverseService
  def call(word)
    sleep word.size * 0.01
    word.reverse
  end
end
