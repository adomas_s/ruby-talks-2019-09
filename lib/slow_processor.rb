require "services/reverse_service"
require "services/uppercase_service"
require "processor_output"

class SlowProcessor
  include ProcessorOutput

  def process(poem)
    @poem = poem
    @result = []

    @benchmark = Benchmark.measure do
      words.each_with_index do |word, i|
        step1 = ReverseService.new.call(word)
        step2 = UppercaseService.new.call(step1)

        result[i] = step2
      end
    end
  end

  private

  attr_reader :benchmark, :poem, :result

  def words
    poem.split(" ")
  end
end
