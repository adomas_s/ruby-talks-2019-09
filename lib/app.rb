require "sinatra/base"
require "sinatra/reloader"
require "json"
require "slim"
require "benchmark"

require "fast_processor"
require "slow_processor"
require "stream_processor"
require "sync_chart_processor"
require "async_chart_processor"

class App < Sinatra::Base
  set :root, -> { File.expand_path(File.join(__dir__, "..")) }
  Slim::Engine.set_options pretty: true

  configure :development do
    register Sinatra::Reloader
    also_reload "lib/**/*.rb"
  end

  get "/" do
    slim :form
  end

  post "/slow-process" do
    process(SlowProcessor)
  end

  post "/fast-process" do
    process(FastProcessor)
  end

  post "/stream-process" do
    content_type("text/plain")

    stream do |output|
      processor = StreamProcessor.new
      processor.process(poem, output)

      output << "Done.\n"

      output << processor.output
    end
  end

  post "/sync-chart" do
    processor = SyncChartProcessor.new
    processor.process(poem)

    js_chart_data = JSON.generate(processor.chart_data)
    js_output = JSON.generate(output: processor.output)

    @load_js = <<~EOL
      SyncChart.load(#{js_chart_data});
      Output.load(#{js_output})
    EOL

    slim :sync_chart
  end

  post "/async-chart" do
    js_args = JSON.generate(
      endpoint: "#{request.base_url}/async-process",
      poem: poem
    );

    @load_js = <<~JS
      AsyncChart.load(#{js_args});
    JS

    slim :async_chart
  end

  post "/async-process" do
    processor = AsyncChartProcessor.new
    stream do |output|
      processor.process(poem, output)
    end
  end

  private

  def poem
    params["poem"] ||
      JSON.parse(request.body.read).fetch("poem")
  end

  def process(processor_class)
    content_type("text/plain")

    processor = processor_class.new
    processor.process(poem)
    processor.output
  end
end
